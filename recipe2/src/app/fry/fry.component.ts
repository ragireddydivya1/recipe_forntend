import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-fry',
  templateUrl: './fry.component.html',
  styleUrl: './fry.component.css'
})
export class FryComponent implements OnInit{
  products:any;
  constructor(private service:CustomerService){
  }
  ngOnInit(): void {
    this.service.getRecipeBycategoryType8().subscribe((data:any)=>{
      this.products=data;
      console.log(data); 
    })
  }
}