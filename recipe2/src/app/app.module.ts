import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { AdminComponent } from './admin/admin.component';
import { AddrecipeComponent } from './addrecipe/addrecipe.component';
import { AdminhomeComponent } from './adminhome/adminhome.component';
import { RecipeComponent } from './recipe/recipe.component';
import { CardsComponent } from './cards/cards.component';
import { HttpClientModule } from '@angular/common/http';
import { VegComponent } from './veg/veg.component';
import { PaneerComponent } from './paneer/paneer.component';
import { BiryaaniComponent } from './biryaani/biryaani.component';
import { ChocolatecakeComponent } from './chocolatecake/Chocolatecake.component';
import { PaneerChettinadComponent } from './paneer-chettinad/paneer-chettinad.component';
import { GulabjamComponent } from './gulabjam/gulabjam.component';
import { PulaoComponent } from './pulao/pulao.component';
import { CustomerhomeComponent } from './customerhome/customerhome.component';
import { ChettinadpotatoComponent } from './chettinadpotato/chettinadpotato.component';
import { ChettinadChickenComponent } from './chettinad-chicken/chettinad-chicken.component';
import { MushroomChettinadComponent } from './mushroom-chettinad/mushroom-chettinad.component';
import { ChettinadMuttonKuzhambuComponent } from './chettinad-mutton-kuzhambu/chettinad-mutton-kuzhambu.component';
import { EverestCholeMasalaComponent } from './everest-chole-masala/everest-chole-masala.component';
import { PavBhajiComponent } from './pav-bhaji/pav-bhaji.component';
import { FishChillyComponent } from './fish-chilly/fish-chilly.component';
import { FishPickleComponent } from './fish-pickle/fish-pickle.component';
import { RaajmaChaavalComponent } from './raajma-chaaval/raajma-chaaval.component';
import { ShahiGattaPulaoComponent } from './shahi-gatta-pulao/shahi-gatta-pulao.component';
import { TomatoRiceComponent } from './tomato-rice/tomato-rice.component';
import { PastaComponent } from './pasta/pasta.component';
import { BroccoliCutletComponent } from './broccoli-cutlet/broccoli-cutlet.component';
import { BroccoliRiceComponent } from './broccoli-rice/broccoli-rice.component';
import { MiniCakesComponent } from './mini-cakes/mini-cakes.component';
import { SandwichCakesComponent } from './sandwich-cakes/sandwich-cakes.component';
import { MirrorGlazeCakeComponent } from './mirror-glaze-cake/mirror-glaze-cake.component';
import { StrawberryCakeComponent } from './strawberry-cake/strawberry-cake.component';
import { ChocolateCakeComponent } from './chocolate-cake/chocolate-cake.component';
import { RedVelvetCakeComponent } from './red-velvet-cake/red-velvet-cake.component';
import { FlipsComponent } from './flips/flips.component';
import { VegBiryaniComponent } from './veg-biryani/veg-biryani.component';
import { VegMomosComponent } from './veg-momos/veg-momos.component';
import { VegKababComponent } from './veg-kabab/veg-kabab.component';
import { PaneerPizzaComponent } from './paneer-pizza/paneer-pizza.component';
import { VegFriedRiceComponent } from './veg-fried-rice/veg-fried-rice.component';
import { VegCheesePizzaComponent } from './veg-cheese-pizza/veg-cheese-pizza.component';
import { ShahiHaleemComponent } from './shahi-haleem/shahi-haleem.component';
import { MuttonHaleemComponent } from './mutton-haleem/mutton-haleem.component';
import { PrawnsBiriyaniComponent } from './prawns-biriyani/prawns-biriyani.component';
import { MuttonBiryaniComponent } from './mutton-biryani/mutton-biryani.component';
import { MasalaCrabsComponent } from './masala-crabs/masala-crabs.component';
import { ChickenPizzaComponent } from './chicken-pizza/chicken-pizza.component';
import { PineappleCakeComponent } from './pineapple-cake/pineapple-cake.component';
import { RasMalaiCakeComponent } from './ras-malai-cake/ras-malai-cake.component';
import { BrownieCakeComponent } from './brownie-cake/brownie-cake.component';
import { FruitcakeComponent } from './fruitcake/fruitcake.component';
import { WatermelonJuiceComponent } from './watermelon-juice/watermelon-juice.component';
import { OrangeJuiceComponent } from './orange-juice/orange-juice.component';
import { PineappleJuiceComponent } from './pineapple-juice/pineapple-juice.component';
import { MangoJuiceComponent } from './mango-juice/mango-juice.component';
import { MangoMilkShakesComponent } from './mango-milk-shakes/mango-milk-shakes.component';
import { OreoMilkShakeComponent } from './oreo-milk-shake/oreo-milk-shake.component';
import { ChocolateMilkShakeComponent } from './chocolate-milk-shake/chocolate-milk-shake.component';
import { ChocolateBananaComponent } from './chocolate-banana/chocolate-banana.component';
import { BadamMilkShakeComponent } from './badam-milk-shake/badam-milk-shake.component';
import { ThandaiIcecreameComponent } from './thandai-icecreame/thandai-icecreame.component';
import { MangoIcecreamComponent } from './mango-icecream/mango-icecream.component';
import { ChocolateIcecreamComponent } from './chocolate-icecream/chocolate-icecream.component';
import { StrawberryIcecreamComponent } from './strawberry-icecream/strawberry-icecream.component';
import { FaloodayIcecreamComponent } from './falooday-icecream/falooday-icecream.component';
import { SamosaComponent } from './samosa/samosa.component';
import { SpicyMixtureComponent } from './spicy-mixture/spicy-mixture.component';
import { PotatoChipsComponent } from './potato-chips/potato-chips.component';
import { PaniPuriComponent } from './pani-puri/pani-puri.component';
import { ChaatComponent } from './chaat/chaat.component';
import { MainPageComponent } from './main-page/main-page.component';
import { HomecardsComponent } from './homecards/homecards.component';
import { CakeComponent } from './cake/cake.component';
import { ShakesComponent } from './shakes/shakes.component';
import { IcecreamsComponent } from './icecreams/icecreams.component';
import { NonvegComponent } from './nonveg/nonveg.component';
import { SnacksComponent } from './snacks/snacks.component';
import { CurriesComponent } from './curries/curries.component';
import { JuiceComponent } from './juice/juice.component';
import { IcecreameComponent } from './icecreame/icecreame.component';
import { SnackItemComponent } from './snack-item/snack-item.component';
import { NonVegComponent } from './non-veg/non-veg.component';
import { CurryComponent } from './curry/curry.component';
import { FryComponent } from './fry/fry.component';
import { SweetComponent } from './sweet/sweet.component';
import { SpecialComponent } from './special/special.component';
import { BreakfastComponent } from './breakfast/breakfast.component';
import { LeftoverComponent } from './leftover/leftover.component';
import { PulihoraComponent } from './pulihora/pulihora.component';
import { DumplingsComponent } from './dumplings/dumplings.component';
import { TomatoDalComponent } from './tomato-dal/tomato-dal.component';
import { GulabjamunComponent } from './gulabjamun/gulabjamun.component';
import { LeftoverRotiFrysComponent } from './leftover-roti-frys/leftover-roti-frys.component';
import { IdlyComponent } from './idly/idly.component';
import { PotatoFryComponent } from './potato-fry/potato-fry.component';
import { PotatoCurryComponent } from './potato-curry/potato-curry.component';
import { EggsCurryComponent } from './eggs-curry/eggs-curry.component';
import { BrinjalCurryComponent } from './brinjal-curry/brinjal-curry.component';
import { BottleGourdCurryComponent } from './bottle-gourd-curry/bottle-gourd-curry.component';
import { ChickenCurryComponent } from './chicken-curry/chicken-curry.component';
import { MuttonCurryComponent } from './mutton-curry/mutton-curry.component';
import { BhindiFryComponent } from './bhindi-fry/bhindi-fry.component';
import { ChickenFryComponent } from './chicken-fry/chicken-fry.component';
import { PrawnsFryComponent } from './prawns-fry/prawns-fry.component';
import { BrinjalFryComponent } from './brinjal-fry/brinjal-fry.component';
import { ShowusersComponent } from './showusers/showusers.component';
import { EditComponent } from './edit/edit.component';
import { DeleteComponent } from './delete/delete.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ShowitemsComponent } from './showitems/showitems.component';
import { FooterComponent } from './footer/footer.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactusComponent } from './contactus/contactus.component';
import { PrivacypolicyComponent } from './privacypolicy/privacypolicy.component';
import { PasswordValidatorDirective } from './password-validator.directive';
import { SampleComponent } from './sample/sample.component';
import { DosaComponent } from './dosa/dosa.component';
import { PooriComponent } from './poori/poori.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    HeaderComponent,
    AdminComponent,
    AddrecipeComponent,
    AdminhomeComponent,
    RecipeComponent,
    CardsComponent,
    VegComponent,
    PaneerComponent,
    BiryaaniComponent,
    ChocolatecakeComponent,
    PaneerChettinadComponent,
    GulabjamComponent,
    PulaoComponent,
    CustomerhomeComponent,
    ChettinadpotatoComponent,
    ChettinadChickenComponent,
    MushroomChettinadComponent,
    ChettinadMuttonKuzhambuComponent,
    EverestCholeMasalaComponent,
    PavBhajiComponent,
    FishChillyComponent,
    FishPickleComponent,
    RaajmaChaavalComponent,
    ShahiGattaPulaoComponent,
    TomatoRiceComponent,
    PastaComponent,
    BroccoliCutletComponent,
    BroccoliRiceComponent,
    MiniCakesComponent,
    SandwichCakesComponent,
    MirrorGlazeCakeComponent,
    StrawberryCakeComponent,
    ChocolateCakeComponent,
    RedVelvetCakeComponent,
    FlipsComponent,
    VegBiryaniComponent,
    VegMomosComponent,
    VegKababComponent,
    PaneerPizzaComponent,
    VegFriedRiceComponent,
    VegCheesePizzaComponent,
    ShahiHaleemComponent,
    MuttonHaleemComponent,
    PrawnsBiriyaniComponent,
    MuttonBiryaniComponent,
    MasalaCrabsComponent,
    ChickenPizzaComponent,
    PineappleCakeComponent,
    RasMalaiCakeComponent,
    BrownieCakeComponent,
    FruitcakeComponent,
    WatermelonJuiceComponent,
    OrangeJuiceComponent,
    PineappleJuiceComponent,
    MangoJuiceComponent,
    MangoMilkShakesComponent,
    OreoMilkShakeComponent,
    ChocolateMilkShakeComponent,
    ChocolateBananaComponent,
    BadamMilkShakeComponent,
    ThandaiIcecreameComponent,
    MangoIcecreamComponent,
    ChocolateIcecreamComponent,
    StrawberryIcecreamComponent,
    FaloodayIcecreamComponent,
    SamosaComponent,
    SpicyMixtureComponent,
    PotatoChipsComponent,
    PaniPuriComponent,
    ChaatComponent,
    MainPageComponent,
    HomecardsComponent,
    CakeComponent,
    ShakesComponent,
    IcecreamsComponent,
    NonvegComponent,
    SnacksComponent,
    CurriesComponent,
    JuiceComponent,
    IcecreameComponent,
    SnackItemComponent,
    NonVegComponent,
    CurryComponent,
    FryComponent,
    SweetComponent,
    SpecialComponent,
    BreakfastComponent,
    LeftoverComponent,
    PulihoraComponent,
    DumplingsComponent,
    TomatoDalComponent,
    GulabjamunComponent,
    LeftoverRotiFrysComponent,
    IdlyComponent,
    PotatoFryComponent,
    PotatoCurryComponent,
    EggsCurryComponent,
    BrinjalCurryComponent,
    BottleGourdCurryComponent,
    ChickenCurryComponent,
    MuttonCurryComponent,
    BhindiFryComponent,
    ChickenFryComponent,
    PrawnsFryComponent,
    BrinjalFryComponent,
    ShowusersComponent,
    EditComponent,
    DeleteComponent,
    DropdownComponent,
    DashboardComponent,
    ShowitemsComponent,
    FooterComponent,
    AboutusComponent,
    ContactusComponent,
    PrivacypolicyComponent,
    PasswordValidatorDirective,
    SampleComponent,
    DosaComponent,
    PooriComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
