import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PulihoraComponent } from './pulihora.component';

describe('PulihoraComponent', () => {
  let component: PulihoraComponent;
  let fixture: ComponentFixture<PulihoraComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PulihoraComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PulihoraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
