import { Injectable } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { CustomerService } from './customer.service';

//export const authGuard: CanActivateFn = (route, state) => {
  //return true;
//};
@Injectable({
  providedIn:'root'
})

export class authGuard {
  constructor(private service:CustomerService, private router:Router) {}

  canActivate(): boolean {
  if(this.service){
    return true;
  } 
else{
  this.router.navigate(["login"])
  return false;
}
  }
}
