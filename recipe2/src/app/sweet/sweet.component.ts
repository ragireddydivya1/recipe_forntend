import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-sweet',
  templateUrl: './sweet.component.html',
  styleUrl: './sweet.component.css'
})
export class SweetComponent implements OnInit {
  products:any;
  constructor(private service:CustomerService){
  }
  ngOnInit(): void {
    this.service.getRecipeBycategoryType9().subscribe((data:any)=>{
      this.products=data;
      console.log(data); 
    })
  }
}