import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-addrecipe',
  templateUrl: './addrecipe.component.html',
  styleUrls: ['./addrecipe.component.css']
})
export class AddrecipeComponent implements OnInit {
  categoryType: any;
  recipeName: any;
  recipeDescription: any;
  cookingTime: any;
  imgUrl: any;

  constructor(private service: CustomerService) { }

  ngOnInit(): void { }

  registerrecipe(formData: any) {
    this.addRecipes(formData);
  }

  addRecipes(form: any) {
    console.log(form);
    const recipeData = {
      categoryType: form.categoryType,
      recipeName: form.recipeName,
      recipeDescription: form.recipeDescription,
      cookingTime: form.cookingTime,
      imgUrl: form.imgUrl
    };
    console.log(recipeData);
    this.service.registerrecipe(recipeData).subscribe((data: any) => {
      console.log(data);
      console.log(recipeData);
    });
  }
}