import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChocolateMilkShakeComponent } from './chocolate-milk-shake.component';

describe('ChocolateMilkShakeComponent', () => {
  let component: ChocolateMilkShakeComponent;
  let fixture: ComponentFixture<ChocolateMilkShakeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChocolateMilkShakeComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ChocolateMilkShakeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
