import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BadamMilkShakeComponent } from './badam-milk-shake.component';

describe('BadamMilkShakeComponent', () => {
  let component: BadamMilkShakeComponent;
  let fixture: ComponentFixture<BadamMilkShakeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BadamMilkShakeComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(BadamMilkShakeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
