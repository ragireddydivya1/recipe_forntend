import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpicyMixtureComponent } from './spicy-mixture.component';

describe('SpicyMixtureComponent', () => {
  let component: SpicyMixtureComponent;
  let fixture: ComponentFixture<SpicyMixtureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SpicyMixtureComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SpicyMixtureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
