import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SandwichCakesComponent } from './sandwich-cakes.component';

describe('SandwichCakesComponent', () => {
  let component: SandwichCakesComponent;
  let fixture: ComponentFixture<SandwichCakesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SandwichCakesComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SandwichCakesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
