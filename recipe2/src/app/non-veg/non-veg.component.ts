import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-non-veg',
  templateUrl: './non-veg.component.html',
  styleUrl: './non-veg.component.css'
})
export class NonVegComponent implements OnInit {
  products:any;
  constructor(private service:CustomerService){
  }
  ngOnInit(): void {
    this.service.getRecipeBycategoryType5().subscribe((data:any)=>{
      this.products=data;
      console.log(data); 
    })
  }
}
