import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BottleGourdCurryComponent } from './bottle-gourd-curry.component';

describe('BottleGourdCurryComponent', () => {
  let component: BottleGourdCurryComponent;
  let fixture: ComponentFixture<BottleGourdCurryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BottleGourdCurryComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(BottleGourdCurryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
