import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChocolateIcecreamComponent } from './chocolate-icecream.component';

describe('ChocolateIcecreamComponent', () => {
  let component: ChocolateIcecreamComponent;
  let fixture: ComponentFixture<ChocolateIcecreamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChocolateIcecreamComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ChocolateIcecreamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
