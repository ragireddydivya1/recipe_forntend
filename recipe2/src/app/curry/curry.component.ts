import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-curry',
  templateUrl: './curry.component.html',
  styleUrl: './curry.component.css'
})
export class CurryComponent implements OnInit {
  products:any;
  constructor(private service:CustomerService){
  }
  ngOnInit(): void {
    this.service.getRecipeBycategoryType7().subscribe((data:any)=>{
      this.products=data;
      console.log(data); 
    })
  }
}