import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShahiHaleemComponent } from './shahi-haleem.component';

describe('ShahiHaleemComponent', () => {
  let component: ShahiHaleemComponent;
  let fixture: ComponentFixture<ShahiHaleemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ShahiHaleemComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ShahiHaleemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
