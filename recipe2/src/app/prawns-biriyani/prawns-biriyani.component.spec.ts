import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrawnsBiriyaniComponent } from './prawns-biriyani.component';

describe('PrawnsBiriyaniComponent', () => {
  let component: PrawnsBiriyaniComponent;
  let fixture: ComponentFixture<PrawnsBiriyaniComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PrawnsBiriyaniComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PrawnsBiriyaniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
