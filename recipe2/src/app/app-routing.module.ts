import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { RecipeComponent } from './recipe/recipe.component';
import { HomeComponent } from './home/home.component';
import { AdminComponent } from './admin/admin.component';
import { AddrecipeComponent } from './addrecipe/addrecipe.component';
import { AdminhomeComponent } from './adminhome/adminhome.component';
import { PulaoComponent } from './pulao/pulao.component';
import { CustomerhomeComponent } from './customerhome/customerhome.component';
import { ChettinadpotatoComponent } from './chettinadpotato/chettinadpotato.component';
import { ChettinadChickenComponent } from './chettinad-chicken/chettinad-chicken.component';
import { MushroomChettinadComponent } from './mushroom-chettinad/mushroom-chettinad.component';
import { ChettinadMuttonKuzhambuComponent } from './chettinad-mutton-kuzhambu/chettinad-mutton-kuzhambu.component';
import { EverestCholeMasalaComponent } from './everest-chole-masala/everest-chole-masala.component';
import { PavBhajiComponent } from './pav-bhaji/pav-bhaji.component';
import { FishChillyComponent } from './fish-chilly/fish-chilly.component';
import { FishPickleComponent } from './fish-pickle/fish-pickle.component';
import { RaajmaChaavalComponent } from './raajma-chaaval/raajma-chaaval.component';
import { ShahiGattaPulaoComponent } from './shahi-gatta-pulao/shahi-gatta-pulao.component';
import { TomatoRiceComponent } from './tomato-rice/tomato-rice.component';
import { PastaComponent } from './pasta/pasta.component';
import { BroccoliCutletComponent } from './broccoli-cutlet/broccoli-cutlet.component';
import { BroccoliRiceComponent } from './broccoli-rice/broccoli-rice.component';
import { MiniCakesComponent } from './mini-cakes/mini-cakes.component';
import { SandwichCakesComponent } from './sandwich-cakes/sandwich-cakes.component';
import { MirrorGlazeCakeComponent } from './mirror-glaze-cake/mirror-glaze-cake.component';
import { StrawberryCakeComponent } from './strawberry-cake/strawberry-cake.component';
import { ChocolateCakeComponent } from './chocolate-cake/chocolate-cake.component';
import { RedVelvetCakeComponent } from './red-velvet-cake/red-velvet-cake.component';
import { VegBiryaniComponent } from './veg-biryani/veg-biryani.component';
import { VegMomosComponent } from './veg-momos/veg-momos.component';
import { VegKababComponent } from './veg-kabab/veg-kabab.component';
import { PaneerPizzaComponent } from './paneer-pizza/paneer-pizza.component';
import { VegFriedRiceComponent } from './veg-fried-rice/veg-fried-rice.component';
import { VegCheesePizzaComponent } from './veg-cheese-pizza/veg-cheese-pizza.component';
import { ShahiHaleemComponent } from './shahi-haleem/shahi-haleem.component';
import { MuttonHaleemComponent } from './mutton-haleem/mutton-haleem.component';
import { PrawnsBiriyaniComponent } from './prawns-biriyani/prawns-biriyani.component';
import { MuttonBiryaniComponent } from './mutton-biryani/mutton-biryani.component';
import { MasalaCrabsComponent } from './masala-crabs/masala-crabs.component';
import { ChickenPizzaComponent } from './chicken-pizza/chicken-pizza.component';
import { PineappleCakeComponent } from './pineapple-cake/pineapple-cake.component';
import { RasMalaiCakeComponent } from './ras-malai-cake/ras-malai-cake.component';
import { BrownieCakeComponent } from './brownie-cake/brownie-cake.component';
import { FruitcakeComponent } from './fruitcake/fruitcake.component';
import { WatermelonJuiceComponent } from './watermelon-juice/watermelon-juice.component';
import { OrangeJuiceComponent } from './orange-juice/orange-juice.component';
import { PineappleJuiceComponent } from './pineapple-juice/pineapple-juice.component';
import { MangoJuiceComponent } from './mango-juice/mango-juice.component';
import { MangoMilkShakesComponent } from './mango-milk-shakes/mango-milk-shakes.component';
import { OreoMilkShakeComponent } from './oreo-milk-shake/oreo-milk-shake.component';
import { ChocolateMilkShakeComponent } from './chocolate-milk-shake/chocolate-milk-shake.component';
import { ChocolateBananaComponent } from './chocolate-banana/chocolate-banana.component';
import { BadamMilkShakeComponent } from './badam-milk-shake/badam-milk-shake.component';
import { ThandaiIcecreameComponent } from './thandai-icecreame/thandai-icecreame.component';
import { MangoIcecreamComponent } from './mango-icecream/mango-icecream.component';
import { ChocolateIcecreamComponent } from './chocolate-icecream/chocolate-icecream.component';
import { StrawberryIcecreamComponent } from './strawberry-icecream/strawberry-icecream.component';
import { FaloodayIcecreamComponent } from './falooday-icecream/falooday-icecream.component';
import { SamosaComponent } from './samosa/samosa.component';
import { SpicyMixtureComponent } from './spicy-mixture/spicy-mixture.component';
import { PotatoChipsComponent } from './potato-chips/potato-chips.component';
import { PaniPuriComponent } from './pani-puri/pani-puri.component';
import { ChaatComponent } from './chaat/chaat.component';
import { MainPageComponent } from './main-page/main-page.component';
import { VegComponent } from './veg/veg.component';
import { CakeComponent } from './cake/cake.component';
import { ShakesComponent } from './shakes/shakes.component';
import { IcecreameComponent } from './icecreame/icecreame.component';
import { SnackItemComponent } from './snack-item/snack-item.component';
import { NonVegComponent } from './non-veg/non-veg.component';
import { JuiceComponent } from './juice/juice.component';
import { CurryComponent } from './curry/curry.component';
import { FryComponent } from './fry/fry.component';
import { SweetComponent } from './sweet/sweet.component';
import { SpecialComponent } from './special/special.component';
import { LeftoverComponent } from './leftover/leftover.component';
import { BreakfastComponent } from './breakfast/breakfast.component';
import { TomatoDalComponent } from './tomato-dal/tomato-dal.component';
import { PotatoFryComponent } from './potato-fry/potato-fry.component';
import { DumplingsComponent } from './dumplings/dumplings.component';
import { LeftoverRotiFrysComponent } from './leftover-roti-frys/leftover-roti-frys.component';
import { IdlyComponent } from './idly/idly.component';
import { GulabjamunComponent } from './gulabjamun/gulabjamun.component';
import { ShowusersComponent } from './showusers/showusers.component';
import { ShowitemsComponent } from './showitems/showitems.component';
import { FooterComponent } from './footer/footer.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactusComponent } from './contactus/contactus.component';
import { PrivacypolicyComponent } from './privacypolicy/privacypolicy.component';
const routes: Routes = [
  { path: '', component: MainPageComponent },
  { path: 'register/login', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'adminhome', component: AdminComponent },
  { path: 'adminhome', component: AdminComponent },
  { path: 'footer', component: FooterComponent },
  { path: 'home', component: HomeComponent },
  { path: 'customerhome', component: CustomerhomeComponent },
  { path: 'customerhome/home', component: HomeComponent },
  { path: 'recipe', component: RecipeComponent },
  { path: 'adminhome/addrecipe', component: AddrecipeComponent },
  { path: 'adminhome', component: AdminhomeComponent },
  { path: 'adminhome/showusers', component: ShowusersComponent },
  { path: 'adminhome/showitems', component: ShowitemsComponent },
  { path: 'home/aboutus', component: AboutusComponent },
  { path: 'recipe', component: RecipeComponent },
  { path: 'home/recipe', component: RecipeComponent },
  { path: 'home/contactus', component: ContactusComponent },
  { path: 'home/privacy-policy', component: PrivacypolicyComponent },
  { path: 'contactus', component: ContactusComponent },
  { path: 'aboutus', component: AboutusComponent },
  { path: 'privacy-policy', component: PrivacypolicyComponent },
  { path: 'veg', component: VegComponent },


  { path: 'home/veg', component: VegComponent },
  { path: 'veg/PavBhaji', component: VegComponent },
  { path: 'veg/MushroomChettinad', component: MushroomChettinadComponent },
  { path: 'veg/EverestCholeMasala', component: EverestCholeMasalaComponent },
  { path: 'veg/ChettinadPotato', component: ChettinadpotatoComponent },
  { path: 'veg/Raajma Chaaval', component: RaajmaChaavalComponent },
  { path: 'veg/ShahiGattaPulao', component: ShahiGattaPulaoComponent },
  { path: 'veg/TomatoRice', component: TomatoRiceComponent },
  { path: 'veg/Pasta', component: PastaComponent },
  { path: 'veg/BroccoliCutlet', component: BroccoliCutletComponent },
  { path: 'veg/BroccoliRice', component: BroccoliRiceComponent },
  { path: 'veg/pulao', component: PulaoComponent },
  { path: 'veg/VegBiryani', component: VegBiryaniComponent },
  { path: 'veg/VegMomos', component: VegMomosComponent },
  { path: 'veg/VegKabab', component: VegKababComponent },
  { path: 'veg/PaneerPizza', component: PaneerPizzaComponent },
  { path: 'veg/VegFriedRice', component: VegFriedRiceComponent },
  { path: 'veg/VegCheesePizza', component: VegCheesePizzaComponent },


  { path: 'home/cake', component: CakeComponent },
  { path: 'home/cake/MiniCakes', component: MiniCakesComponent },
  { path: 'home/cake/MiniCakes', component: MiniCakesComponent },
  { path: 'home/cake/SandwichCakes', component: SandwichCakesComponent },
  { path: 'home/cake/MirrorGlazeCake', component: MirrorGlazeCakeComponent },
  { path: 'home/cake/StrawberryCake', component: StrawberryCakeComponent },
  { path: 'home/cake/ChocolateCake', component: ChocolateCakeComponent },
  { path: 'home/cake/RedVelvetCake', component: RedVelvetCakeComponent },
  { path: 'home/cake/PineappleCake', component: PineappleCakeComponent },
  { path: 'home/cake/RasMalaiCake', component: RasMalaiCakeComponent },
  { path: 'home/cake/BrownieCake', component: BrownieCakeComponent },


  { path: 'home/shakes', component: ShakesComponent },
  { path: 'home/shakes/MangoMilkShakes', component: MangoMilkShakesComponent },
  { path: 'home/shakes/MangoMilkShakes', component: MangoMilkShakesComponent },
  { path: 'home/shakes/OreoMilkShake', component: OreoMilkShakeComponent },
  { path: 'home/shakes/ChocolateMilkShake', component: ChocolateMilkShakeComponent },
  { path: 'home/shakes/ChocolateBanana', component: ChocolateBananaComponent },
  { path: 'home/shakes/BadamMilkShake', component: BadamMilkShakeComponent },


  { path: 'home/juice', component: JuiceComponent },
  { path: 'home/juice/WatermelonJuice', component: WatermelonJuiceComponent },
  { path: 'home/juice/OrangeJuice', component: OrangeJuiceComponent },
  { path: 'home/juice/PineappleJuice', component: PineappleJuiceComponent },
  { path: 'home/juice/MangoJuice', component: MangoJuiceComponent },


  { path: 'home/icecreame', component: IcecreameComponent },
  { path: 'home/icecreame/ThandaiIcecreame', component: ThandaiIcecreameComponent },
  { path: 'home/icecreame/MangoIcecream', component: MangoIcecreamComponent },
  { path: 'home/icecreame/ChocolateIcecream', component: ChocolateIcecreamComponent },
  { path: 'home/icecreame/StrawberryIcecream', component: StrawberryIcecreamComponent },
  { path: 'home/icecreame/FaloodayIcecream', component: FaloodayIcecreamComponent },


  { path: 'home/non-veg', component: NonVegComponent },
  { path: 'home/non-veg/ChettinadChicken', component: ChettinadChickenComponent },
  { path: 'home/non-veg/FishChilly', component: FishChillyComponent },
  { path: 'home/non-veg/FishPickle', component: FishPickleComponent },
  { path: 'home/non-veg/ShahiHaleem', component: ShahiHaleemComponent },
  { path: 'home/non-veg/MuttonHaleem', component: MuttonHaleemComponent },
  { path: 'home/non-veg/PrawnsBiriyani', component: PrawnsBiriyaniComponent },
  { path: 'home/non-veg/MuttonBiryani', component: MuttonBiryaniComponent },
  { path: 'home/non-veg/MasalaCrabs', component: MasalaCrabsComponent },
  { path: 'home/non-veg/ChickenPizza', component: ChickenPizzaComponent },


  { path: 'home/snack-item', component: SnackItemComponent },
  { path: 'home/snack-item/Samosa', component: SamosaComponent },
  { path: 'home/snack-item/SpicyMixture', component: SpicyMixtureComponent },
  { path: 'home/snack-item/PotatoChips', component: PotatoChipsComponent },
  { path: 'home/snack-item/PaniPuri', component: PaniPuriComponent },
  { path: 'home/snack-item/Chaat', component: ChaatComponent },


  { path: 'home/curry', component: CurryComponent },
  { path: 'home/curry/TomatoDal', component: TomatoDalComponent },



  { path: 'home/fry', component: FryComponent },
  { path: 'home/fry/PotatoFry', component: PotatoFryComponent },

  { path: 'home/sweet/', component: SweetComponent },
  { path: 'home/sweet/Gulabjamun', component: GulabjamunComponent },


  { path: 'home/special', component: SpecialComponent },
  { path: 'home/special/Dumplings', component: DumplingsComponent },

  { path: 'home/leftover', component: LeftoverComponent },
  { path: 'home/leftover/LeftoverRotiFrys', component: LeftoverRotiFrysComponent },

  { path: 'home/breakfast', component: BreakfastComponent },
  { path: 'home/breakfast/Idly', component: IdlyComponent },







  { path: 'home/ChettinadMuttonKuzhambuComponent', component: ChettinadMuttonKuzhambuComponent },
  { path: 'home/MushroomChettinad', component: MushroomChettinadComponent },
  { path: 'home/ChettinadChicken', component: ChettinadChickenComponent },
  { path: 'home/ChettinadPotato', component: ChettinadpotatoComponent },
  { path: 'home/PavBhaji', component: PavBhajiComponent },
  { path: 'home/FishChilly', component: FishChillyComponent },
  { path: 'home/ShahiGattaPulao', component: ShahiGattaPulaoComponent },
  { path: 'home/TomatoRice', component: TomatoRiceComponent },
  { path: 'home/Pasta', component: PastaComponent },
  { path: 'home/BroccoliCutlet', component: BroccoliCutletComponent },
  { path: 'home/BroccoliRice', component: BroccoliRiceComponent },
  { path: 'home/pulao', component: PulaoComponent },
  { path: 'home/MiniCakes', component: MiniCakesComponent },
  { path: 'home/SandwichCakes', component: SandwichCakesComponent },
  { path: 'home/MirrorGlazeCake', component: MirrorGlazeCakeComponent },
  { path: 'home/StrawberryCake', component: StrawberryCakeComponent },
  { path: 'home/ChocolateCake', component: ChocolateCakeComponent },
  { path: 'home/RedVelvetCake', component: RedVelvetCakeComponent },
  { path: 'home/VegBiryani', component: VegBiryaniComponent },
  { path: 'home/VegMomos', component: VegMomosComponent },
  { path: 'home/VegKabab', component: VegKababComponent },
  { path: 'home/PaneerPizza', component: PaneerPizzaComponent },
  { path: 'home/VegFriedRice', component: VegFriedRiceComponent },
  { path: 'home/VegCheesePizza', component: VegCheesePizzaComponent },
  { path: 'home/PineappleCake', component: PineappleCakeComponent },
  { path: 'home/RasMalaiCake', component: RasMalaiCakeComponent },
  { path: 'home/BrownieCake', component: BrownieCakeComponent },
  { path: 'home/Fruitcake', component: FruitcakeComponent },
  { path: 'home/WatermelonJuice', component: WatermelonJuiceComponent },
  { path: 'home/OrangeJuice', component: OrangeJuiceComponent },
  { path: 'home/PineappleJuice', component: PineappleJuiceComponent },
  { path: 'home/MangoJuice', component: MangoJuiceComponent },
  { path: 'home/MangoMilkShakes', component: MangoMilkShakesComponent },
  { path: 'home/OreoMilkShake', component: OreoMilkShakeComponent },
  { path: 'home/ChocolateMilkShake', component: ChocolateMilkShakeComponent },
  { path: 'home/ChocolateBanana', component: ChocolateBananaComponent },
  { path: 'home/BadamMilkShake', component: BadamMilkShakeComponent },
  { path: 'home/ThandaiIcecreame', component: ThandaiIcecreameComponent },
  { path: 'home/MangoIcecream', component: MangoIcecreamComponent },
  { path: 'home/ChocolateIcecream', component: ChocolateIcecreamComponent },
  { path: 'home/StrawberryIcecream', component: StrawberryIcecreamComponent },
  { path: 'home/FaloodayIcecream', component: FaloodayIcecreamComponent },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
