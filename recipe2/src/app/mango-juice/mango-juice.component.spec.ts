import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MangoJuiceComponent } from './mango-juice.component';

describe('MangoJuiceComponent', () => {
  let component: MangoJuiceComponent;
  let fixture: ComponentFixture<MangoJuiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MangoJuiceComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MangoJuiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
