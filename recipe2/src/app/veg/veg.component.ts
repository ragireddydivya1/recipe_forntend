import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-veg',
  templateUrl: './veg.component.html',
  styleUrl: './veg.component.css'
})
export class VegComponent implements OnInit {
    products:any;
    constructor(private service:CustomerService){
    }
    ngOnInit(): void {
      this.service.getRecipeBycategoryType().subscribe((data:any)=>{
        this.products=data;
        console.log(data); 
      })
    }
}
