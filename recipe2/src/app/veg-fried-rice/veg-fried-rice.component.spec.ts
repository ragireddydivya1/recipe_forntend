import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VegFriedRiceComponent } from './veg-fried-rice.component';

describe('VegFriedRiceComponent', () => {
  let component: VegFriedRiceComponent;
  let fixture: ComponentFixture<VegFriedRiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VegFriedRiceComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(VegFriedRiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
