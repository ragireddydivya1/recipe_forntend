import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MangoIcecreamComponent } from './mango-icecream.component';

describe('MangoIcecreamComponent', () => {
  let component: MangoIcecreamComponent;
  let fixture: ComponentFixture<MangoIcecreamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MangoIcecreamComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MangoIcecreamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
