import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlipsComponent } from './flips.component';

describe('FlipsComponent', () => {
  let component: FlipsComponent;
  let fixture: ComponentFixture<FlipsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FlipsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FlipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
