import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaniPuriComponent } from './pani-puri.component';

describe('PaniPuriComponent', () => {
  let component: PaniPuriComponent;
  let fixture: ComponentFixture<PaniPuriComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PaniPuriComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PaniPuriComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
