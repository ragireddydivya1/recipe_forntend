import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MuttonCurryComponent } from './mutton-curry.component';

describe('MuttonCurryComponent', () => {
  let component: MuttonCurryComponent;
  let fixture: ComponentFixture<MuttonCurryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MuttonCurryComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MuttonCurryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
