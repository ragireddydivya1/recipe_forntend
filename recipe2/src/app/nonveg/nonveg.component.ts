import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-nonveg',
  templateUrl: './nonveg.component.html',
  styleUrl: './nonveg.component.css'
})
export class NonvegComponent implements OnInit {
  products:any;
  constructor(private service:CustomerService){
  }
  ngOnInit(): void {
    this.service.getRecipeBycategoryType().subscribe((data:any)=>{
      this.products=data;
      console.log(data); 
    })
  }
}
{

}
