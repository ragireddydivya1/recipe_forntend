import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FaloodayIcecreamComponent } from './falooday-icecream.component';

describe('FaloodayIcecreamComponent', () => {
  let component: FaloodayIcecreamComponent;
  let fixture: ComponentFixture<FaloodayIcecreamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FaloodayIcecreamComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FaloodayIcecreamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
