import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChocolateCakeComponent } from './chocolate-cake.component';

describe('ChocolateCakeComponent', () => {
  let component: ChocolateCakeComponent;
  let fixture: ComponentFixture<ChocolateCakeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChocolateCakeComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ChocolateCakeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
