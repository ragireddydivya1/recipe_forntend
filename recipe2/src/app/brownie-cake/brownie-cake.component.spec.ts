import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BrownieCakeComponent } from './brownie-cake.component';

describe('BrownieCakeComponent', () => {
  let component: BrownieCakeComponent;
  let fixture: ComponentFixture<BrownieCakeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BrownieCakeComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(BrownieCakeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
