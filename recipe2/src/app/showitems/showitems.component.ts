import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-showitems',
  templateUrl: './showitems.component.html',
  styleUrl: './showitems.component.css'
})
export class ShowitemsComponent implements OnInit{
  Items : any;
  constructor(private service:CustomerService){
  }

  ngOnInit(): void {
    this.loadItems();
  }

  loadItems(): void {
    this.service.getAllItems().subscribe((data: any) => {
      this.Items = data;
    });
  }

  updateItem(item: any): void {
    this.service.updateItem(item).subscribe(() => {
      this.loadItems();
    });
  }

  deleteItem(id: number): void {
    this.service.deleteItem(id).subscribe(() => {
      this.loadItems();
    });
  }
}

