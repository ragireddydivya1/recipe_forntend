import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TomatoDalComponent } from './tomato-dal.component';

describe('TomatoDalComponent', () => {
  let component: TomatoDalComponent;
  let fixture: ComponentFixture<TomatoDalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TomatoDalComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TomatoDalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
