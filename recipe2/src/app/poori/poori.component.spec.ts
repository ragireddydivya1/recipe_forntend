import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PooriComponent } from './poori.component';

describe('PooriComponent', () => {
  let component: PooriComponent;
  let fixture: ComponentFixture<PooriComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PooriComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PooriComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
