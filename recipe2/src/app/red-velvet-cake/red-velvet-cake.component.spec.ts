import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RedVelvetCakeComponent } from './red-velvet-cake.component';

describe('RedVelvetCakeComponent', () => {
  let component: RedVelvetCakeComponent;
  let fixture: ComponentFixture<RedVelvetCakeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RedVelvetCakeComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(RedVelvetCakeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
