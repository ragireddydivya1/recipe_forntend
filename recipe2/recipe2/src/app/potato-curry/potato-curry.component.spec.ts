import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PotatoCurryComponent } from './potato-curry.component';

describe('PotatoCurryComponent', () => {
  let component: PotatoCurryComponent;
  let fixture: ComponentFixture<PotatoCurryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PotatoCurryComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PotatoCurryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
