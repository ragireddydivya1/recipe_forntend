import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-shakes',
  templateUrl: './shakes.component.html',
  styleUrl: './shakes.component.css'
})
export class ShakesComponent implements   OnInit{
  products:any;
  constructor(private service:CustomerService){
  }
  ngOnInit(): void {
    this.service.getRecipeBycategoryType3().subscribe((data:any)=>{
      this.products=data;
      console.log(data); 
    })

  }
}
