import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GulabjamunComponent } from './gulabjamun.component';

describe('GulabjamunComponent', () => {
  let component: GulabjamunComponent;
  let fixture: ComponentFixture<GulabjamunComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GulabjamunComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(GulabjamunComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
