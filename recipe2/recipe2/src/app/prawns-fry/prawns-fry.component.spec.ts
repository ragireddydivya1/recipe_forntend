import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrawnsFryComponent } from './prawns-fry.component';

describe('PrawnsFryComponent', () => {
  let component: PrawnsFryComponent;
  let fixture: ComponentFixture<PrawnsFryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PrawnsFryComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PrawnsFryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
