import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PotatoChipsComponent } from './potato-chips.component';

describe('PotatoChipsComponent', () => {
  let component: PotatoChipsComponent;
  let fixture: ComponentFixture<PotatoChipsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PotatoChipsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PotatoChipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
