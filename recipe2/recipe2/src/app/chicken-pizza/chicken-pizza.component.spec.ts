import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChickenPizzaComponent } from './chicken-pizza.component';

describe('ChickenPizzaComponent', () => {
  let component: ChickenPizzaComponent;
  let fixture: ComponentFixture<ChickenPizzaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChickenPizzaComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ChickenPizzaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
