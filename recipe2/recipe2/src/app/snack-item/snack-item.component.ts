import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-snack-item',
  templateUrl: './snack-item.component.html',
  styleUrl: './snack-item.component.css'
})
export class SnackItemComponent implements OnInit {
  products:any;
  constructor(private service:CustomerService){
  }
  ngOnInit(): void {
    this.service.getRecipeBycategoryType6().subscribe((data:any)=>{
      this.products=data;
      console.log(data); 
    })
  }
}
