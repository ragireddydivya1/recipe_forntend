import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MasalaCrabsComponent } from './masala-crabs.component';

describe('MasalaCrabsComponent', () => {
  let component: MasalaCrabsComponent;
  let fixture: ComponentFixture<MasalaCrabsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MasalaCrabsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MasalaCrabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
