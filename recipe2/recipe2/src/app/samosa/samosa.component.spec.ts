import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SamosaComponent } from './samosa.component';

describe('SamosaComponent', () => {
  let component: SamosaComponent;
  let fixture: ComponentFixture<SamosaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SamosaComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SamosaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
