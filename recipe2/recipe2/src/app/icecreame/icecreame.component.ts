import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-icecreame',
  templateUrl: './icecreame.component.html',
  styleUrl: './icecreame.component.css'
})
export class IcecreameComponent implements OnInit{
  products:any;
  constructor(private service:CustomerService){
  }
  ngOnInit(): void {
    this.service.getRecipeBycategoryType4().subscribe((data:any)=>{
      this.products=data;
      console.log(data); 
    })
  }
}
