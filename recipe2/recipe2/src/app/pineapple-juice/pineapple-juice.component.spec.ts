import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PineappleJuiceComponent } from './pineapple-juice.component';

describe('PineappleJuiceComponent', () => {
  let component: PineappleJuiceComponent;
  let fixture: ComponentFixture<PineappleJuiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PineappleJuiceComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PineappleJuiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
