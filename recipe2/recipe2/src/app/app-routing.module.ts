import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { RecipeComponent } from './recipe/recipe.component';
import { HomeComponent } from './home/home.component';
import { AdminComponent } from './admin/admin.component';
import { AddrecipeComponent } from './addrecipe/addrecipe.component';
import { AdminhomeComponent } from './adminhome/adminhome.component';
import { BiryaaniComponent } from './biryaani/biryaani.component';
import { PaneerChettinadComponent } from './paneer-chettinad/paneer-chettinad.component';
import { GulabjamComponent } from './gulabjam/gulabjam.component';
import { PulaoComponent } from './pulao/pulao.component';
import { CustomerhomeComponent } from './customerhome/customerhome.component';
import { ChettinadpotatoComponent } from './chettinadpotato/chettinadpotato.component';
import { ChettinadChickenComponent } from './chettinad-chicken/chettinad-chicken.component';
import { MushroomChettinadComponent } from './mushroom-chettinad/mushroom-chettinad.component';
import { ChettinadMuttonKuzhambuComponent } from './chettinad-mutton-kuzhambu/chettinad-mutton-kuzhambu.component';
import { EverestCholeMasalaComponent } from './everest-chole-masala/everest-chole-masala.component';
import { PavBhajiComponent } from './pav-bhaji/pav-bhaji.component';
import { FishChillyComponent } from './fish-chilly/fish-chilly.component';
import { FishPickleComponent } from './fish-pickle/fish-pickle.component';
import { RaajmaChaavalComponent } from './raajma-chaaval/raajma-chaaval.component';
import { ShahiGattaPulaoComponent } from './shahi-gatta-pulao/shahi-gatta-pulao.component';
import { TomatoRiceComponent } from './tomato-rice/tomato-rice.component';
import { PastaComponent } from './pasta/pasta.component';
import { BroccoliCutletComponent } from './broccoli-cutlet/broccoli-cutlet.component';
import { BroccoliRiceComponent } from './broccoli-rice/broccoli-rice.component';
import { MiniCakesComponent } from './mini-cakes/mini-cakes.component';
import { SandwichCakesComponent } from './sandwich-cakes/sandwich-cakes.component';
import { MirrorGlazeCakeComponent } from './mirror-glaze-cake/mirror-glaze-cake.component';
import { StrawberryCakeComponent } from './strawberry-cake/strawberry-cake.component';
import { ChocolateCakeComponent } from './chocolate-cake/chocolate-cake.component';
import { RedVelvetCakeComponent } from './red-velvet-cake/red-velvet-cake.component';
import { VegBiryaniComponent } from './veg-biryani/veg-biryani.component';
import { VegMomosComponent } from './veg-momos/veg-momos.component';
import { VegKababComponent } from './veg-kabab/veg-kabab.component';
import { PaneerPizzaComponent } from './paneer-pizza/paneer-pizza.component';
import { VegFriedRiceComponent } from './veg-fried-rice/veg-fried-rice.component';
import { VegCheesePizzaComponent } from './veg-cheese-pizza/veg-cheese-pizza.component';
import { ShahiHaleemComponent } from './shahi-haleem/shahi-haleem.component';
import { MuttonHaleemComponent } from './mutton-haleem/mutton-haleem.component';
import { PrawnsBiriyaniComponent } from './prawns-biriyani/prawns-biriyani.component';
import { MuttonBiryaniComponent } from './mutton-biryani/mutton-biryani.component';
import { MasalaCrabsComponent } from './masala-crabs/masala-crabs.component';
import { ChickenPizzaComponent } from './chicken-pizza/chicken-pizza.component';
import { PineappleCakeComponent } from './pineapple-cake/pineapple-cake.component';
import { RasMalaiCakeComponent } from './ras-malai-cake/ras-malai-cake.component';
import { BrownieCakeComponent } from './brownie-cake/brownie-cake.component';
import { FruitcakeComponent } from './fruitcake/fruitcake.component';
import { WatermelonJuiceComponent } from './watermelon-juice/watermelon-juice.component';
import { OrangeJuiceComponent } from './orange-juice/orange-juice.component';
import { PineappleJuiceComponent } from './pineapple-juice/pineapple-juice.component';
import { MangoJuiceComponent } from './mango-juice/mango-juice.component';
import { MangoMilkShakesComponent } from './mango-milk-shakes/mango-milk-shakes.component';
import { OreoMilkShakeComponent } from './oreo-milk-shake/oreo-milk-shake.component';
import { ChocolateMilkShakeComponent } from './chocolate-milk-shake/chocolate-milk-shake.component';
import { ChocolateBananaComponent } from './chocolate-banana/chocolate-banana.component';
import { BadamMilkShakeComponent } from './badam-milk-shake/badam-milk-shake.component';
import { ThandaiIcecreameComponent } from './thandai-icecreame/thandai-icecreame.component';
import { MangoIcecreamComponent } from './mango-icecream/mango-icecream.component';
import { ChocolateIcecreamComponent } from './chocolate-icecream/chocolate-icecream.component';
import { StrawberryIcecreamComponent } from './strawberry-icecream/strawberry-icecream.component';
import { FaloodayIcecreamComponent } from './falooday-icecream/falooday-icecream.component';
import { SamosaComponent } from './samosa/samosa.component';
import { SpicyMixtureComponent } from './spicy-mixture/spicy-mixture.component';
import { PotatoChipsComponent } from './potato-chips/potato-chips.component';
import { PaniPuriComponent } from './pani-puri/pani-puri.component';
import { ChaatComponent } from './chaat/chaat.component';
import { MainPageComponent } from './main-page/main-page.component';
import { HomecardsComponent } from './homecards/homecards.component';
import { VegComponent } from './veg/veg.component';
import { CakeComponent } from './cake/cake.component';
import { ShakesComponent } from './shakes/shakes.component';
import { NonvegComponent } from './nonveg/nonveg.component';
import { IcecreamsComponent } from './icecreams/icecreams.component';
import { SnacksComponent } from './snacks/snacks.component';
import { IcecreameComponent } from './icecreame/icecreame.component';
import { SnackItemComponent } from './snack-item/snack-item.component';
import { NonVegComponent } from './non-veg/non-veg.component';
import { JuiceComponent } from './juice/juice.component';
import { CurryComponent } from './curry/curry.component';
import { FryComponent } from './fry/fry.component';
import { SweetComponent } from './sweet/sweet.component';
import { SpecialComponent } from './special/special.component';
import { LeftoverComponent } from './leftover/leftover.component';
import { BreakfastComponent } from './breakfast/breakfast.component';
import { TomatoDalComponent } from './tomato-dal/tomato-dal.component';
import { PotatoFryComponent } from './potato-fry/potato-fry.component';
import { DumplingsComponent } from './dumplings/dumplings.component';
import { LeftoverRotiFrysComponent } from './leftover-roti-frys/leftover-roti-frys.component';
import { IdlyComponent } from './idly/idly.component';
import { GulabjamunComponent } from './gulabjamun/gulabjamun.component';

const routes: Routes = [
  { path: '', component: MainPageComponent },
  { path: 'register/login', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'adminhome', component: AdminComponent },
  { path: 'home', component: HomeComponent },
  { path: 'customerhome', component: CustomerhomeComponent },
  { path: 'customerhome/home', component: HomeComponent },
  { path: 'customerhome/home/homecards/veg', component: HomecardsComponent },
  { path: 'recipe', component: RecipeComponent },
  { path: 'adminhome/addrecipe', component: AddrecipeComponent },
  { path: 'adminhome', component: AdminhomeComponent },
  { path: 'adminhome/recipe', component: RecipeComponent },
  { path: 'recipe/biryaani', component: BiryaaniComponent },
  { path: 'customerhome/home/veg', component: VegComponent },
  { path: 'customerhome/home/veg/ChettinadPotato', component: ChettinadpotatoComponent },
  { path: 'customerhome/home/cake', component: CakeComponent },
  { path: 'customerhome/home/cake/MiniCakes', component: MiniCakesComponent },
  { path: 'customerhome/home/shakes', component: ShakesComponent },
  { path: 'customerhome/home/shakes/MangoMilkShakes', component: MangoMilkShakesComponent },

  { path: 'customerhome/home/juice', component: JuiceComponent },
  { path: 'customerhome/home/juice/WatermelonJuice', component: WatermelonJuiceComponent },


  { path: 'customerhome/home/icecreame', component: IcecreameComponent },
  { path: 'customerhome/home/icecreame/ThandaiIcecreame', component: ThandaiIcecreameComponent },

  { path: 'customerhome/home/non-veg', component: NonVegComponent },
  { path: 'customerhome/home/non-veg/ChettinadChicken', component: ChettinadChickenComponent },

  { path: 'customerhome/home/Snack-Item', component: SnackItemComponent },

  { path: 'customerhome/home/curry', component: CurryComponent },
  { path: 'customerhome/home/curry/TomatoDal', component: TomatoDalComponent },

  { path: 'customerhome/home/fry', component: FryComponent },
  { path: 'customerhome/home/fry/PotatoFry', component: PotatoFryComponent },

  { path: 'customerhome/home/sweet/', component: SweetComponent },
  { path: 'customerhome/home/sweet/Gulabjamun', component: GulabjamunComponent },


  { path: 'customerhome/home/special', component: SpecialComponent },
  { path: 'customerhome/home/special/Dumplings', component: DumplingsComponent },

  { path: 'customerhome/home/leftover', component: LeftoverComponent },
  { path: 'customerhome/home/leftover/LeftoverRotiFrys', component: LeftoverRotiFrysComponent },

  { path: 'customerhome/home/breakfast', component: BreakfastComponent },
  { path: 'customerhome/home/breakfast/Idly', component: IdlyComponent },
  //{path:'customerhome/home/curries',component:CakeComponent},
  // { path: 'customerhome/home/cake', component: MiniCakesComponent},
  { path: 'customerhome/home/veg/PavBhaji', component: VegComponent },
  { path: 'customerhome/recipe', component: RecipeComponent },
  { path: 'customerhome/recipe/PaneerChettinad', component: PaneerChettinadComponent },
  { path: 'customerhome/recipe/gulabjam', component: GulabjamComponent },
  { path: 'customerhome/recipe/ChettinadPotato', component: ChettinadpotatoComponent },
  { path: 'customerhome/recipe/ChettinadChicken', component: ChettinadChickenComponent },
  { path: 'customerhome/recipe/MushroomChettinad', component: MushroomChettinadComponent },
  { path: 'customerhome/recipe/ChettinadMuttonKuzhambu', component: ChettinadMuttonKuzhambuComponent },
  { path: 'customerhome/recipe/EverestCholeMasala', component: EverestCholeMasalaComponent },
  { path: 'customerhome/recipe/PavBhaji', component: PavBhajiComponent },
  { path: 'customerhome/recipe/FishChilly', component: FishChillyComponent },
  { path: 'customerhome/recipe/FishPickle', component: FishPickleComponent },
  { path: 'customerhome/recipe/Raajma Chaaval', component: RaajmaChaavalComponent },
  { path: 'customerhome/recipe/ShahiGattaPulao', component: ShahiGattaPulaoComponent },
  { path: 'customerhome/recipe/TomatoRice', component: TomatoRiceComponent },
  { path: 'customerhome/recipe/Pasta', component: PastaComponent },
  { path: 'customerhome/recipe/BroccoliCutlet', component: BroccoliCutletComponent },
  { path: 'customerhome/recipe/BroccoliRice', component: BroccoliRiceComponent },
  { path: 'customerhome/recipe/pulao', component: PulaoComponent },
  { path: 'customerhome/recipe/MiniCakes', component: MiniCakesComponent },
  { path: 'customerhome/recipe/SandwichCakes', component: SandwichCakesComponent },
  { path: 'customerhome/recipe/MirrorGlazeCake', component: MirrorGlazeCakeComponent },
  { path: 'customerhome/recipe/StrawberryCake', component: StrawberryCakeComponent },
  { path: 'customerhome/recipe/ChocolateCake', component: ChocolateCakeComponent },
  { path: 'customerhome/recipe/RedVelvetCake', component: RedVelvetCakeComponent },
  { path: 'customerhome/recipe/VegBiryani', component: VegBiryaniComponent },
  { path: 'customerhome/recipe/VegMomos', component: VegMomosComponent },
  { path: 'customerhome/recipe/VegKabab', component: VegKababComponent },
  { path: 'customerhome/recipe/PaneerPizza', component: PaneerPizzaComponent },
  { path: 'customerhome/recipe/VegFriedRice', component: VegFriedRiceComponent },
  { path: 'customerhome/recipe/VegCheesePizza', component: VegCheesePizzaComponent },
  { path: 'customerhome/recipe/ShahiHaleem', component: ShahiHaleemComponent },
  { path: 'customerhome/recipe/MuttonHaleem', component: MuttonHaleemComponent },
  { path: 'customerhome/recipe/PrawnsBiriyani', component: PrawnsBiriyaniComponent },
  { path: 'customerhome/recipe/MuttonBiryani', component: MuttonBiryaniComponent },
  { path: 'customerhome/recipe/MasalaCrabs', component: MasalaCrabsComponent },
  { path: 'customerhome/recipe/ChickenPizza', component: ChickenPizzaComponent },
  { path: 'customerhome/recipe/PineappleCake', component: PineappleCakeComponent },
  { path: 'customerhome/recipe/RasMalaiCake', component: RasMalaiCakeComponent },
  { path: 'customerhome/recipe/BrownieCake', component: BrownieCakeComponent },
  { path: 'customerhome/recipe/Fruitcake', component: FruitcakeComponent },
  { path: 'customerhome/recipe/WatermelonJuice', component: WatermelonJuiceComponent },
  { path: 'customerhome/recipe/OrangeJuice', component: OrangeJuiceComponent },
  { path: 'customerhome/recipe/PineappleJuice', component: PineappleJuiceComponent },
  { path: 'customerhome/recipe/MangoJuice', component: MangoJuiceComponent },
  { path: 'customerhome/recipe/MangoMilkShakes', component: MangoMilkShakesComponent },
  { path: 'customerhome/recipe/OreoMilkShake', component: OreoMilkShakeComponent },
  { path: 'customerhome/recipe/ChocolateMilkShake', component: ChocolateMilkShakeComponent },
  { path: 'customerhome/recipe/ChocolateBanana', component: ChocolateBananaComponent },
  { path: 'customerhome/recipe/BadamMilkShake', component: BadamMilkShakeComponent },
  { path: 'customerhome/recipe/ThandaiIcecreame', component: ThandaiIcecreameComponent },
  { path: 'customerhome/recipe/MangoIcecream', component: MangoIcecreamComponent },
  { path: 'customerhome/recipe/ChocolateIcecream', component: ChocolateIcecreamComponent },
  { path: 'customerhome/recipe/StrawberryIcecream', component: StrawberryIcecreamComponent },
  { path: 'customerhome/recipe/FaloodayIcecream', component: FaloodayIcecreamComponent },
  { path: 'customerhome/recipe/Samosa', component: SamosaComponent },
  { path: 'customerhome/recipe/SpicyMixture', component: SpicyMixtureComponent },
  { path: 'customerhome/recipe/PotatoChips', component: PotatoChipsComponent },
  { path: 'customerhome/recipe/PaniPuri', component: PaniPuriComponent },
  { path: 'customerhome/recipe/Chaat', component: ChaatComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
