import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WatermelonJuiceComponent } from './watermelon-juice.component';

describe('WatermelonJuiceComponent', () => {
  let component: WatermelonJuiceComponent;
  let fixture: ComponentFixture<WatermelonJuiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WatermelonJuiceComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(WatermelonJuiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
