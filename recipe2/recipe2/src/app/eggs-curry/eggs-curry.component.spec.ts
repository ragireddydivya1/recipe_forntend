import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EggsCurryComponent } from './eggs-curry.component';

describe('EggsCurryComponent', () => {
  let component: EggsCurryComponent;
  let fixture: ComponentFixture<EggsCurryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EggsCurryComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(EggsCurryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
