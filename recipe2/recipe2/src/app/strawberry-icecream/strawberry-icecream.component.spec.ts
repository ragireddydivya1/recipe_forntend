import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StrawberryIcecreamComponent } from './strawberry-icecream.component';

describe('StrawberryIcecreamComponent', () => {
  let component: StrawberryIcecreamComponent;
  let fixture: ComponentFixture<StrawberryIcecreamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StrawberryIcecreamComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(StrawberryIcecreamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
