import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChickenFryComponent } from './chicken-fry.component';

describe('ChickenFryComponent', () => {
  let component: ChickenFryComponent;
  let fixture: ComponentFixture<ChickenFryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChickenFryComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ChickenFryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
