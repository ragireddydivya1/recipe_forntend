import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaneerPizzaComponent } from './paneer-pizza.component';

describe('PaneerPizzaComponent', () => {
  let component: PaneerPizzaComponent;
  let fixture: ComponentFixture<PaneerPizzaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PaneerPizzaComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PaneerPizzaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
