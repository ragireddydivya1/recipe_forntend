import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VegMomosComponent } from './veg-momos.component';

describe('VegMomosComponent', () => {
  let component: VegMomosComponent;
  let fixture: ComponentFixture<VegMomosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VegMomosComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(VegMomosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
