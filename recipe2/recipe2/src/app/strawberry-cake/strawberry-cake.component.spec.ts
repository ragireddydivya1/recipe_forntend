import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StrawberryCakeComponent } from './strawberry-cake.component';

describe('StrawberryCakeComponent', () => {
  let component: StrawberryCakeComponent;
  let fixture: ComponentFixture<StrawberryCakeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StrawberryCakeComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(StrawberryCakeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
