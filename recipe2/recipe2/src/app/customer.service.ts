import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class CustomerService {
  @Injectable({
    providedIn: 'root'
  })
   isUserLoggedIn : boolean
  constructor(private http: HttpClient) { 
    this.isUserLoggedIn=false;
  }

registerCustomer(customerData: any):any{
    return this.http.post("http://localhost:8080/register", customerData);
}
getCustomers():any{
return this.http.get('http://localhost:8080/getAllCustomers');
}
getAllItems():any{
  return this.http.get('http://localhost:8080/getAllItems');

}
registerrecipe(recipeData:any):any{
  return this.http.post('http://localhost:8080/registerrecipe',recipeData);
}
getRecipeBycategoryType():any{
  return this.http.get('http://localhost:8080/getByRecipeCategoryName/veg');
}
getRecipeBycategoryType1():any{
  return this.http.get('http://localhost:8080/getByRecipeCategoryName1/cake');
}
getRecipeBycategoryType2():any{
  return this.http.get('http://localhost:8080/getByRecipeCategoryName2/juice');
}
getRecipeBycategoryType3():any{
  return this.http.get('http://localhost:8080/getByRecipeCategoryName3/shakes');
}
getRecipeBycategoryType4():any{
  return this.http.get('http://localhost:8080/getByRecipeCategoryName4/icecreame');
}
getRecipeBycategoryType5():any{
  return this.http.get('http://localhost:8080/getByRecipeCategoryName5/non-veg');
}
getRecipeBycategoryType6():any{
  return this.http.get('http://localhost:8080/getByRecipeCategoryName6/snacks-items');
}
getRecipeBycategoryType7():any{
  return this.http.get('http://localhost:8080/getByRecipeCategoryName7/curry');
}
getRecipeBycategoryType8():any{
  return this.http.get('http://localhost:8080/getByRecipeCategoryName8/fry');
}
getRecipeBycategoryType9():any{
  return this.http.get('http://localhost:8080/getByRecipeCategoryName9/sweet');
}
getRecipeBycategoryType10():any{
  return this.http.get('http://localhost:8080/getByRecipeCategoryName10/special');
}
getRecipeBycategoryType11():any{
  return this.http.get('http://localhost:8080/getByRecipeCategoryName11/leftover');
}
getRecipeBycategoryType12():any{
  return this.http.get('http://localhost:8080/getByRecipeCategoryName12/breakfast');
}
}
