import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PineappleCakeComponent } from './pineapple-cake.component';

describe('PineappleCakeComponent', () => {
  let component: PineappleCakeComponent;
  let fixture: ComponentFixture<PineappleCakeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PineappleCakeComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PineappleCakeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
