import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit{

  firstname: any;
  lastname: any;
  email: any;
  password: any;

  constructor(private service: CustomerService) {}

  ngOnInit(): void {}

  Register(form: any) {
    const customerData = {
      firstName: form.firstname,
      lastName: form.lastname,
      email: form.email,
      password: form.password,
    };

    console.log(form);

    this.service.registerCustomer(customerData).subscribe((data: any) => {
      console.log(data);
    });
  }
}







