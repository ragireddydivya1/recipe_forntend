import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BhindiFryComponent } from './bhindi-fry.component';

describe('BhindiFryComponent', () => {
  let component: BhindiFryComponent;
  let fixture: ComponentFixture<BhindiFryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BhindiFryComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(BhindiFryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
