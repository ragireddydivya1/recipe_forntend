import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-special',
  templateUrl: './special.component.html',
  styleUrl: './special.component.css'
})
export class SpecialComponent implements OnInit{
  products:any;
  constructor(private service:CustomerService){
  }
  ngOnInit(): void {
    this.service.getRecipeBycategoryType10().subscribe((data:any)=>{
      this.products=data;
      console.log(data); 
    })
  }
}