import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-cake',
  templateUrl: './cake.component.html',
  styleUrl: './cake.component.css'
})
export class CakeComponent  implements OnInit {
    products:any;
    constructor(private service:CustomerService){
    }
    ngOnInit(): void {
      this.service.getRecipeBycategoryType1().subscribe((data:any)=>{
        this.products=data;
        console.log(data); 
      })
    }
}



