package com.user;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.dao.RecipeDao;
import com.model.Recipe;
@RestController
@CrossOrigin(origins="*")
public class RecipeController {
	
	@Autowired
	private RecipeDao recipeDao;
	
	@GetMapping("/getAllItems")
	public List<Recipe> getAllItems(){
		List<Recipe> eList = recipeDao.getAllItems();
    	System.out.println(eList);
        return eList;
	}
	@PostMapping("/registerrecipe")
    public void register(@RequestBody Recipe item) {
        System.out.println("Data Received: "+item);
        recipeDao.register(item);
    }
	@GetMapping("/getByRecipeId/{id}")
    public Recipe getItemById(@PathVariable int id) {
        return recipeDao.getItemById(id);
    }
	
	@GetMapping("/getByRecipeName/{recipeName}")
    public Recipe getRecipeByName(@PathVariable String recipeName){
    	return recipeDao.getRecipeByName(recipeName);
    }
    
	@GetMapping("/getByRecipeCategoryName/{categoryType}")
	public List<Recipe> getRecipeByCategory(@PathVariable String categoryType){
	    return recipeDao.getRecipeBycategoryType(categoryType);
	}
	@GetMapping("/getByRecipeCategoryName1/{categoryType}")
    public List<Recipe> getRecipeByCategory1(@PathVariable String categoryType){
    	return recipeDao.getRecipeBycategoryType1(categoryType);
    }
	@GetMapping("/getByRecipeCategoryName2/{categoryType}")
    public List<Recipe> getRecipeByCategory2(@PathVariable String categoryType){
    	return recipeDao.getRecipeBycategoryType2(categoryType);
    }
	@GetMapping("/getByRecipeCategoryName3/{categoryType}")
    public List<Recipe> getRecipeByCategory3(@PathVariable String categoryType){
    	return recipeDao.getRecipeBycategoryType3(categoryType);
    }
	@GetMapping("/getByRecipeCategoryName4/{categoryType}")
    public List<Recipe> getRecipeByCategory4(@PathVariable String categoryType){
    	return recipeDao.getRecipeBycategoryType4(categoryType);
    }
	@GetMapping("/getByRecipeCategoryName5/{categoryType}")
    public List<Recipe> getRecipeByCategory5(@PathVariable String categoryType){
    	return recipeDao.getRecipeBycategoryType5(categoryType);
    }
	@GetMapping("/getByRecipeCategoryName6/{categoryType}")
    public List<Recipe> getRecipeByCategory6(@PathVariable String categoryType){
    	return recipeDao.getRecipeBycategoryType6(categoryType);
    }
	@GetMapping("/getByRecipeCategoryName7/{categoryType}")
    public List<Recipe> getRecipeByCategory7(@PathVariable String categoryType){
    	return recipeDao.getRecipeBycategoryType7(categoryType);
    }
	@GetMapping("/getByRecipeCategoryName8/{categoryType}")
    public List<Recipe> getRecipeByCategory8(@PathVariable String categoryType){
    	return recipeDao.getRecipeBycategoryType8(categoryType);
    }
	@GetMapping("/getByRecipeCategoryName9/{categoryType}")
    public List<Recipe> getRecipeByCategory9(@PathVariable String categoryType){
    	return recipeDao.getRecipeBycategoryType9(categoryType);
    }
	@GetMapping("/getByRecipeCategoryName10/{categoryType}")
    public List<Recipe> getRecipeByCategory10(@PathVariable String categoryType){
    	return recipeDao.getRecipeBycategoryType10(categoryType);
    }
	@GetMapping("/getByRecipeCategoryName11/{categoryType}")
    public List<Recipe> getRecipeByCategory11(@PathVariable String categoryType){
    	return recipeDao.getRecipeBycategoryType11(categoryType);
    }
	@GetMapping("/getByRecipeCategoryName12/{categoryType}")
    public List<Recipe> getRecipeByCategory12(@PathVariable String categoryType){
    	return recipeDao.getRecipeBycategoryType12(categoryType);
    }
	
	
	 @DeleteMapping("/recipedelete/{id}")
	    public void deleteItemById(@PathVariable int id) {
	         recipeDao.deleteItemById(id);
	    }
	    
	 @PutMapping("/updaterecipe")
	    public Recipe updateItem(@RequestBody Recipe item){
			return recipeDao.updateItem(item);
	    }
    

}
