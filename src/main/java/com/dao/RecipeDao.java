package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Recipe;
@Service
public class RecipeDao {
@Autowired
	private RecipeRepo recipeRepo;

public List<Recipe> getAllItems(){
	return recipeRepo.findAll();
}
public void register(Recipe item){
	recipeRepo.save(item);
}

public Recipe getItemById(int id) {
	// TODO Auto-generated method stub
	return recipeRepo.findById(id).orElse(new Recipe());
}

public Recipe getRecipeByName(String recipeName){
	return recipeRepo.findByRecipeName(recipeName);
}

public List<Recipe> getRecipeBycategoryType(String categoryType){
	return recipeRepo.findBycategoryType(categoryType);
}
public List<Recipe> getRecipeBycategoryType1(String categoryType){
	return recipeRepo.findBycategoryType(categoryType);
}
public List<Recipe> getRecipeBycategoryType2(String categoryType){
	return recipeRepo.findBycategoryType(categoryType);
}
public List<Recipe> getRecipeBycategoryType3(String categoryType){
	return recipeRepo.findBycategoryType(categoryType);
}
public List<Recipe> getRecipeBycategoryType4(String categoryType){
	return recipeRepo.findBycategoryType(categoryType);
}
public List<Recipe> getRecipeBycategoryType5(String categoryType){
	return recipeRepo.findBycategoryType(categoryType);
}
public List<Recipe> getRecipeBycategoryType6(String categoryType){
	return recipeRepo.findBycategoryType(categoryType);
}
public List<Recipe> getRecipeBycategoryType7(String categoryType){
	return recipeRepo.findBycategoryType(categoryType);
}
public List<Recipe> getRecipeBycategoryType8(String categoryType){
	return recipeRepo.findBycategoryType(categoryType);
}
public List<Recipe> getRecipeBycategoryType9(String categoryType){
	return recipeRepo.findBycategoryType(categoryType);
}
public List<Recipe> getRecipeBycategoryType10(String categoryType){
	return recipeRepo.findBycategoryType(categoryType);
}
public List<Recipe> getRecipeBycategoryType11(String categoryType){
	return recipeRepo.findBycategoryType(categoryType);
}
public List<Recipe> getRecipeBycategoryType12(String categoryType){
	return recipeRepo.findBycategoryType(categoryType);
}


public void deleteItemById(int id){
	recipeRepo.deleteById(id);
	
}

public Recipe updateItem(Recipe item){
	Recipe existingItem = recipeRepo.findById(item.getId()).orElse(null);
   
   if (existingItem != null) {
      existingItem.setRecipeName(item.getRecipeName());
       existingItem.setCategoryType(item.getCategoryType());
        return recipeRepo.save(existingItem);
        } else {
       return null;
    }
	
}}
	
